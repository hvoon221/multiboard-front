import * as React from 'react';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import {useEffect, useState} from "react";
import {boards} from "../../api/member/BoardApi";
import {Board} from "../../api/member/model/Board";
import Title from "../layout/Title";
import {Button, Container, Paper, Grid, Toolbar, Box} from '@mui/material';

const goRegister = async (event: React.MouseEvent) => {

}

export const Detail = (row) => {

    const [ boardList, setBoardList ] = useState<Board[]>([]);

    useEffect(() => {
        boards().then(res => setBoardList(res))
    }, [])

    function goBoard(event: React.MouseEvent) {
        event.preventDefault();
    }

    return (
        <React.Fragment >
            <Title>{row.title}</Title>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>Title</TableCell>
                        <TableCell>Content</TableCell>
                        <TableCell>Date</TableCell>
                        <TableCell>Writer</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {boardList.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell>
                                <Link color="primary" onClick={goBoard} sx={{ mt: 3 }}>
                                    {row.title}
                                </Link>
                            </TableCell>
                            <TableCell>
                                <Link color="primary" onClick={goBoard} sx={{ mt: 3 }}>
                                    {row.content}
                                </Link>
                            </TableCell>
                            <TableCell>{}</TableCell>
                            <TableCell>{row.writer}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <Button
                href="/register"
                onClick={goRegister}
                sx={{
                    mt: 3,
                    backgroundColor:'darkgray',
                    color:'white',
                    width: '100px',
                }}>
                등록
            </Button>
        </React.Fragment>
    );
}