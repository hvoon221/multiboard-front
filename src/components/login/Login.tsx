import * as React from 'react';
import {login} from "../../api/member/MemberApi";
import {AxiosError} from "axios";
import {useState} from "react";
import {ErrorResponse} from "../../api/shared/model/ErrorResponse";
import {useNavigate} from "react-router";
import {
    createTheme, ThemeProvider,
    Toolbar,
    Paper,
    Grid,
    Box,
    Container,
    Button,
    TextField,
    CssBaseline, Typography,
} from "@mui/material";

const theme = createTheme();

export default function Login() {

    const [userid, setUserid] = useState('');
    const [pw, setPw] = useState('');

    const navigate = useNavigate();

    const handleSubmit = () => {
        //
        if (!userid) {
            alert('아이디를 입력하세요.');
            return;
        }

        if (!pw) {
            alert('비밀번호를 입력하세요.');
            return;
        }

        login(userid as string, pw as string)
            .then((member) => {
                alert(member.name + '님 환영합니다.');
                navigate('/');
            })
            .catch((exception: AxiosError<ErrorResponse>) => {
                alert(exception.response?.data?.message);
            });
    };

    return (
            <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                <Grid item xs={12}>
                    <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                    <ThemeProvider theme={theme}>
                    <Container component="main" maxWidth="xs">
                        <CssBaseline />
                        <Box
                            sx={{
                                marginTop: 8,
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'center',
                            }}
                        >
                            <Typography component="h1" variant="h3">
                                Multi-Board
                            </Typography>
                            <Box sx={{ mt: 1 }}>
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="userId"
                                    label="Id"
                                    name="userId"
                                    autoComplete="userId"
                                    autoFocus
                                    value={userid}
                                    onChange={e => setUserid(e.target.value)}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Password"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                    value={pw}
                                    onChange={e => setPw(e.target.value)}
                                />
                                <Button
                                    onClick={handleSubmit}
                                    fullWidth
                                    variant="contained"
                                    sx={{ mt: 3, mb: 2, backgroundColor:'darkgray', fontSize:'120%', width:'200px'}}
                                >
                                    로그인
                                </Button>
                            </Box>
                        </Box>
                    </Container>
                    </ThemeProvider>
                    </Paper>
                </Grid>
            </Container>
    );
}