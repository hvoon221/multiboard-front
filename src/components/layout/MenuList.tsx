import * as React from 'react';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ContentPasteIcon from '@mui/icons-material/ContentPaste';
import QuestionMarkIcon from '@mui/icons-material/QuestionMark';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import QuestionAnswerOutlinedIcon from '@mui/icons-material/QuestionAnswerOutlined';
import {useNavigate} from "react-router";
export const MenuList = () => {

    const  navigate = useNavigate();

    return (
        <React.Fragment>
            <ListItemButton onClick={() => navigate('/general')}>
                <ListItemIcon>
                    <ContentPasteIcon />
                </ListItemIcon>
                <ListItemText primary="General" />
            </ListItemButton>
            <ListItemButton onClick={() => navigate('/notice')}>
                <ListItemIcon>
                    <NotificationsNoneIcon />
                </ListItemIcon>
                <ListItemText primary="Notice" />
            </ListItemButton>
            <ListItemButton onClick={() => navigate('/faq')}>
                <ListItemIcon>
                    <QuestionAnswerOutlinedIcon />
                </ListItemIcon>
                <ListItemText primary="FAQ" />
            </ListItemButton>
            <ListItemButton onClick={() => navigate('/qna')}>
                <ListItemIcon>
                    <QuestionMarkIcon />
                </ListItemIcon>
                <ListItemText primary="Q&A" />
            </ListItemButton>
        </React.Fragment>
    )
};
