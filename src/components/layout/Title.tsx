
import * as React from 'react';
import Typography from '@mui/material/Typography';

interface TitleProps {
    children?: React.ReactNode;
}

export default function Title(props: TitleProps) {
    return (
        <Typography variant="h3" align="left" gutterBottom>
            {props.children}
        </Typography>
    );
}