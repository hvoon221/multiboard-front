import * as React from 'react';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import { MenuList } from './MenuList';
import {General} from "../board/General";
import {
    styled,
    createTheme,
    ThemeProvider,
    CssBaseline,
    Box,
    Toolbar,
    Container,
    List,
    Typography,
    Paper,
    Grid,
    Button,
} from "@mui/material";
import {PropsWithChildren} from "react";

interface AppBarProps extends MuiAppBarProps {
    open?: boolean;
}

const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme }) => ({
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
}));

const mdTheme = createTheme({
    palette:{
        primary:{
            main:'#9a9da6',
        }
    }
});

const Layout = ({children}: PropsWithChildren) => {
    return (
        <ThemeProvider theme={mdTheme}>
            <Box sx={{ display: 'flex' }}>
                <CssBaseline />
                <AppBar position="absolute">
                    <Typography
                        // component="h1"
                        variant="h2"
                        color="inherit"
                        noWrap
                        sx={{
                            flexGrow: 1,
                            backgroundColor: 'darkgray',
                        }}
                    >
                        Multi-Board
                    </Typography>
                </AppBar>

                {/* menu list */}
                <List
                    // component="nav"
                    sx={{
                        padding: '90px 40px 0px 20px',
                    }}
                >
                    {MenuList()}
                </List>
                <Box
                    component="main"
                    sx={{
                        backgroundColor: (theme) =>
                            theme.palette.mode === 'light'
                                ? theme.palette.grey[100]
                                : theme.palette.grey[900],
                        flexGrow: 1,
                        height: '100vh',
                        overflow: 'auto',
                        width:'1000vh',
                    }}
                >
                    <Toolbar />
                    <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                        <Grid item xs={12}>
                            <Paper sx={{ p: 2, display: 'flex', flexDirection: 'column' }}>
                                {children}
                            </Paper>
                        </Grid>
                    </Container>
                </Box>
            </Box>
        </ThemeProvider>

    );
}

export default Layout;