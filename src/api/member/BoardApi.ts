import {Board} from "./model/Board";
import axios from "axios";

export const boards = async (): Promise<Board[]> => {
    const res =  await axios.get('http://localhost:5173/api/board/')
    return res.data;
}