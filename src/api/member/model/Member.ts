import {Entity} from "../../shared/model/Entity";

export interface Member extends Entity {
    userid: string,
    name: string,
    pw: string,
}