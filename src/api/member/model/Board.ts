import {Entity} from "../../shared/model/Entity";

export interface Board extends Entity{
    title: string,
    content: string,
    boardType: string,
    writer: string,

}