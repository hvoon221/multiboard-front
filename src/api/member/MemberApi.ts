import axios from 'axios';
import {LoginDto} from "./model/vo/LoginDto";
import {Member} from "./model/Member";



export const login = (userid: string, pw: string) => {
    const loginDto: LoginDto = { userid, pw };
    return axios.post<Member>(
        'http://localhost:5173/api/member/login',
            loginDto,
        ).then(res => res.data)
}

