import { useState } from 'react'
import './App.css'
import {BrowserRouter, Route, Routes} from "react-router-dom";

//
import Layout from "./components/layout/Layout";

// detail, register, edit
import {Detail} from "./components/board/Detail";
import {Register} from "./components/board/Register";

// menu
import {General} from "./components/board/General";
import {Notice} from "./components/board/Notice";
import {Faq} from "./components/board/Faq";
import {Qna} from "./components/board/Qna";


function App() {
  const [count, setCount] = useState(0)

  return (
    <div className="App">
        <BrowserRouter>
            {/*<Route path='/login' element={<Login />}/>*/}
            <Layout>
                <Routes>
                    <Route path='/register' element={<Register/>}/>
                    <Route path='/:general' element={<General/>}/>
                    <Route path='/:notice' element={<Notice/>}/>
                    <Route path='/:faq' element={<Faq/>}/>
                    <Route path='/:qna' element={<Qna/>}/>
                    <Route path='/:detail' element={<Detail/>}/>
                </Routes>
            </Layout>
        </BrowserRouter>
    </div>
  )
}

export default App
